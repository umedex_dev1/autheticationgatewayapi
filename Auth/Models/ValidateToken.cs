﻿namespace AuthenticationGatewayApi.Auth.Models
{
    public class ValidateToken
    {
        public required string AccessToken { get; set; }
    }
}
