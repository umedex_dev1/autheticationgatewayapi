﻿namespace AuthenticationGatewayApi.Logger.Interface
{
    public interface IRequestResponseLogging
    {
        Task Invoke(HttpContext context);
    }
}
