﻿namespace AuthenticationGatewayApi.Logger.Interface
{
    public interface IApplicationLogger
    {
        Task Log(HttpContext context, string logMessage);
        Task WriteToLog(string dir, string file, string content);
    }
}
