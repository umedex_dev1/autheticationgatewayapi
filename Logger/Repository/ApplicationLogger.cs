﻿using AuthenticationGatewayApi.Logger.Interface;
using Microsoft.Net.Http.Headers;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;
namespace AuthenticationGatewayApi.Logger.Repository
{
    public class ApplicationLogger : IApplicationLogger
    {
        public async Task Log(HttpContext context, string logMessage)
        {
            try
            {
                var accessToken = context.Request.Headers[HeaderNames.Authorization];
                var folderDate = DateTime.Now.ToString("dd-MMM-yyyy");
                if (AuthenticationHeaderValue.TryParse(accessToken, out var headerValue))
                {
                    var jwt = new JwtSecurityTokenHandler().ReadJwtToken(headerValue.Parameter);
                    string userName = jwt.Claims.First(c => c.Type == ClaimTypes.Name).Value;
                    string dirPath = Environment.CurrentDirectory + "\\logs\\" + folderDate + "\\";
                    if (!Directory.Exists(dirPath))
                    {
                        Directory.CreateDirectory(dirPath);
                    }
                    string fileName = Path.Combine(dirPath, userName + "_" + DateTime.Now.ToString("dd-MMM-yyyy") + ".txt");
                    await WriteToLog(dirPath, fileName, logMessage);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
        public async Task WriteToLog(string dir, string file, string content)
        {
            using (StreamWriter outputFile = new StreamWriter(Path.Combine(dir, file), true))
            {
                await outputFile.WriteLineAsync(string.Format("Logged on: {1} at: {2}{0}Message: {3} at {4}{0}--------------------{0}",
                          Environment.NewLine, DateTime.Now.ToLongDateString(),
                          DateTime.Now.ToLongTimeString(), content, DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss.fff")));
            }
        }
    }
}
