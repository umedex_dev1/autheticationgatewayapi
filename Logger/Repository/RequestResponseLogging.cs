﻿using AuthenticationGatewayApi.Logger.Interface;
using System.Text;
namespace AuthenticationGatewayApi.Logger.Repository
{
    public class RequestResponseLogging
    {
        private readonly RequestDelegate _next;
        private readonly IApplicationLogger applicationLogger;
        public RequestResponseLogging(RequestDelegate next, IApplicationLogger _applicationLogger)
        {
            _next = next;
            applicationLogger = _applicationLogger;
        }
        public async Task Invoke(HttpContext context)
        {
            await LogRequest(context);
            var originalResponseBody = context.Response.Body;
            using (var responseBody = new MemoryStream())
            {
                context.Response.Body = responseBody;
                await _next.Invoke(context);

                await LogResponse(context, responseBody, originalResponseBody);
            }
        }
        private async Task LogResponse(HttpContext context, MemoryStream responseBody, Stream originalResponseBody)
        {
            var responseContent = new StringBuilder();
            responseContent.AppendLine("=== Response Info ===");

            responseContent.AppendLine("-- headers");
            foreach (var (headerKey, headerValue) in context.Response.Headers)
            {
                responseContent.AppendLine($"header = {headerKey}    value = {headerValue}");
            }
            responseContent.AppendLine($"statuscode = {context.Response.StatusCode}");
            responseContent.AppendLine("-- body");
            responseBody.Position = 0;
            var content = await new StreamReader(responseBody).ReadToEndAsync();
            responseContent.AppendLine($"body = {content}");
            responseBody.Position = 0;
            await responseBody.CopyToAsync(originalResponseBody);
            context.Response.Body = originalResponseBody;
            _ = applicationLogger.Log(context, responseContent.ToString());
        }
        private async Task LogRequest(HttpContext context)
        {
            var requestContent = new StringBuilder();
            requestContent.AppendLine("=== Request Info ===");
            requestContent.AppendLine($"method = {context.Request.Method.ToUpper()}");
            requestContent.AppendLine($"path = {context.Request.Path}");
            requestContent.AppendLine($"host: {context.Request.Host}");
            requestContent.AppendLine($"Content-Type: {context.Request.ContentType}");
            requestContent.AppendLine($"Content-Length: {context.Request.ContentLength}");
            requestContent.AppendLine("-- headers");
            foreach (var (headerKey, headerValue) in context.Request.Headers)
            {
                requestContent.AppendLine($"header = {headerKey}    value = {headerValue}");
            }
            requestContent.AppendLine("-- body");
            context.Request.EnableBuffering();
            var requestReader = new StreamReader(context.Request.Body);
            var content = await requestReader.ReadToEndAsync();
            requestContent.AppendLine($"body = {content}");
            _ = applicationLogger.Log(context, requestContent.ToString());
            context.Request.Body.Position = 0;
        }
    }
}
